Webinar: Monitoring Kubernetes Clusters with Prometheus

This session will describe the integration of Prometheus with our Container Service,
and how it can offer admins an overview of their clusters from the general status down
to performance details about their deployments.

In this hands-on session we will learn:

* How to launch a kubernetes cluster with monitoring enabled
    * What is included in the monitoring stack: prometheus, alertmanager, grafana, prometheus-adapter
    * Overview of how prometheus works
* Launch a demo application
    * Gather custom metrics exposed from the application
    * Creating custom prometheus rules to manipulate existing metrics
    * Creating and managing alarms
* Auto scaling using the HPA (horizontal pod auto-scaler) and custom prometheus metrics

More info: https://indico.cern.ch/event/915706/

This is part of a Web Series on Container Clusters. Check all the available resources
at https://clouddocs.web.cern.ch/containers/training.html
Videos Available Here: https://www.youtube.com/channel/UCvFftdXaeIJLua2wVoKWz6A
